# mvi-sp

Zadání:

Zadáním mé semestrální práce je záměna vlajek ve videu, kde jsem se zaměřil na záměnu vlajek ve formě nášivek na vojenských uniformách. Zvolil jsem vlajky USA a SRN. Tedy je třeba zaměnit nášivku tak, aby byl výsledek co nejvíce uvěřitelný.

Data:

Přiloženy zde v adresáři "datasets".

Spuštění:

Práce je ve formě jupyter notebooku. Hlavní část práce je v notebooku "pipeline", část práce je v separátním notebooku "maskRCNN" z důvodu nekompatibilty verzí tensorflow. V notebooku "pipeline" je uvedeno, kdy je třeba přejít k notebooku "maskRCNN".

Dále jsou zde notebooky ke trénování jednotlivých části modelu, tedy YOLOv3, mask RCNN a cycle-GAN.